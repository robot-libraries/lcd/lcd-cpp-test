//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: init_fini.cpp
//
//     Author: rmerriam
//
//    Created: Jan 2, 2022
//
//======================================================================================================================
#include <thread>
#include <iostream>
#include <LcdStream.h>
//----------------------------------------------------------------------------------------------------------------------
void display_move(lcd::LcdStream const& lcd) {
    lcd.clear();
    lcd.cursor();
    lcd.blink();

    lcd.move(0, 2);
    lcd.write("Shift display left");
    std::this_thread::sleep_for(std::chrono::milliseconds(750));
    for (int i {}; i < 5; ++i) {
        lcd.displayLeft();
        std::this_thread::sleep_for(std::chrono::milliseconds(750));
    }
    lcd.displayLeft(5);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    lcd.move(0, 3);
    lcd.write("Shift display right");
    std::this_thread::sleep_for(std::chrono::milliseconds(750));
    for (int i {}; i < 5; ++i) {
        lcd.displayRight();
        std::this_thread::sleep_for(std::chrono::milliseconds(750));
    }
    lcd.displayRight(5);
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    lcd.cursor(false);
    lcd.blink(false);
}
//----------------------------------------------------------------------------------------------------------------------
static void write_test_text(lcd::LcdStream const& lcd) {
    static char const text[] { "0123456789abcdefgh" };
    for (int i { 0 }; text[i] != 0; ++i) {
        lcd.write(text[i]);
        std::this_thread::sleep_for(std::chrono::milliseconds(750));
    }
}
//----------------------------------------------------------------------------------------------------------------------
void cursor_move(lcd::LcdStream const& lcd) {
    lcd.clear();
    lcd.cursor();

    lcd.write("Move cursor 5 left");
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    lcd.left(5);
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));

    lcd.clear();
    lcd.write("Move cursor 5 right");
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    lcd.right(5);
    std::this_thread::sleep_for(std::chrono::milliseconds(1500));

    lcd.clear();
    lcd.write(" Down diagonal & up");
    lcd.blink();

    // move diagonally down from home and back up
    for (uint8_t i {}; i < 6; ++i) {
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
        if (i > 3) {
            lcd.move(i, 6 - i);
        }
        else {
            lcd.move(i, i);
        }
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));

    // move down right column
    lcd.clear();
    lcd.write("Move down right col");
    for (uint8_t i {}; i < 4; ++i) {
        lcd.move(19, i);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }

//    lcd.cursor(false);
//    lcd.blink(false);
//    lcd.clear();
//    lcd.write("Write right to left");
//
//    lcd.move(10, 2);
//    lcd.textLtoR(false);
//    write_test_text(lcd);
//
//    lcd.clear();
//    lcd.textLtoR();
//    lcd.write("Write left to right");
//    lcd.move(10, 1);
//    write_test_text(lcd);

    lcd.clear();
    lcd.write("Scroll right to left");
    lcd.scroll(false);
    lcd.move(10, 2);
    write_test_text(lcd);

    lcd.clear();
    lcd.write("Scroll left to right");
    lcd.scroll();
    lcd.move(10, 1);
    write_test_text(lcd);

    lcd.clear();
    lcd.cursor(false);
    lcd.blink(false);
}
