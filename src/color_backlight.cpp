//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: init_fini.cpp
//
//     Author: rmerriam
//
//    Created: Jan 2, 2022
//
//======================================================================================================================
#include <thread>

#include <LcdStream.h>
//----------------------------------------------------------------------------------------------------------------------
void color_changes(lcd::LcdStream const& lcd) {
//    lcd.blink();

    lcd.clear();
    lcd.write("Red");
    lcd.color(255, 0, 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.write("Green");
    lcd.color(0, 255, 0);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.write("Blue");
    lcd.color(0, 0, 255);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.write("Mid Gray");
    lcd.color(128, 128, 128);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.write("Low Gray");
    lcd.color(64, 64, 64);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.write("High Gray");
    lcd.color(64 + 128, 64 + 128, 64 + 128);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.write("Max Gray");
    lcd.color(255, 255, 255);
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

}
//----------------------------------------------------------------------------------------------------------------------
void contrast_changes(lcd::LcdStream const& lcd) {
    lcd.clear();
    lcd.contrast();
    lcd.write("Default Contrast: 40");
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.contrast(20);
    lcd.write("Contrast: 20");
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.contrast(100);
    lcd.write("Contrast: 100");
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.clear();
    lcd.contrast();
}
//----------------------------------------------------------------------------------------------------------------------
void backlight(lcd::LcdStream const& lcd) {
    lcd.contrast();

    lcd.color(255, 0, 0);
    for (auto i { 0 }; i < 256; i += (256 / 4) - 1) {
        lcd.clear();
        lcd.redIntensity(i);
        lcd.write("Red Intensity: ");
        lcd.write(std::to_string(i));
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }

    lcd.color(0, 255, 0);
    for (auto i { 0 }; i < 256; i += (256 / 4) - 1) {
        lcd.clear();
        lcd.greenIntensity(i);
        lcd.write("Green: ");
        lcd.write(std::to_string(i));
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }

    lcd.color(0, 0, 255);
    for (auto i { 0 }; i < 256; i += (256 / 4) - 1) {
        lcd.clear();
        lcd.blueIntensity(i);
        lcd.write("Blue: ");
        lcd.write(std::to_string(i));
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
    }
    lcd.color(255, 255, 255);
    lcd.clear();
}

