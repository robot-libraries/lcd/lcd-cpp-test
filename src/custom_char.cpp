//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: custom_char.cpp
//
//     Author: rmerriam
//
//    Created: Jan 4, 2022
//
//======================================================================================================================
#include <thread>

#include <LcdStream.h>
//----------------------------------------------------------------------------------------------------------------------
void custom_char(lcd::LcdStream const&lcd) {
    enum drawing_chars : uint8_t {
        top_bar= 0,    //
            left_up= 1, left_down= 2, left_bar= 3,    //
            right_up= 4, right_down= 5, right_bar= 6,    //
            bottom_bar= 7,
    };

    lcd.clear();
    lcd.cursor(false);
    lcd.blink(false);

    lcd::LcdStream::char_map c[9] {    //
    { 0x1F, 0x00, 0x0, 0x00, 0x00, 0x00, 0x00, 0x00 },    // 0 top bar
        //
        { 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x1F },    // 1 left up
        { 0x1F, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10 },    // 2 left down
        { 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10, 0x10 },    // 3 left bar
        //
        { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x1F },    // 4 right up
        { 0x1F, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 },    //  5 right down
        { 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01 },    // 6 right bar
        //
        { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1F },    // 7 bottom bar
    };

    lcd.write("Creating custom chars");
    for(auto i { 0 }; i< 8; ++i) {
        lcd.createChar(i, c[i]);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(2000));

    uint8_t row {};
    uint8_t col {};
    uint8_t draw[][3] {    //
    { col, row, left_down }, { ++col, row, top_bar }, { ++col, row, right_down },    //
    { (col= 0), ++row, left_bar }, { ++++col, row, right_bar },    //
        { (col= 0), ++row, left_bar }, { ++++col, row, right_bar },    //
        { (col= 0), ++row, left_up }, { ++col, row, bottom_bar }, { ++col, row, right_up },    //
    };

    lcd.clear();
    lcd.move(5, 3);
    lcd.write("Drawing chars");

    for(auto x : draw) {
        lcd.move(x[0], x[1]);
        lcd.writeChar(x[2]);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));

    lcd.move(10, 0);
    lcd.writeChar(left_down);
    lcd.writeChar(top_bar);
    lcd.writeChar(right_down);

    lcd.move(10, 1);
    lcd.writeChar(left_bar);
    lcd.write(" ");
    lcd.writeChar(right_bar);

    std::this_thread::sleep_for(std::chrono::milliseconds(13000));
}
