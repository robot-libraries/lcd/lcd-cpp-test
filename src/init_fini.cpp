//======================================================================================================================
// 2022 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: init_fini.cpp
//
//     Author: rmerriam
//
//    Created: Jan 2, 2022
//
//======================================================================================================================
#include <thread>

#include <LcdStream.h>
//----------------------------------------------------------------------------------------------------------------------
void init(lcd::LcdStream const& lcd) {
    lcd.display();
    lcd.clear();
    lcd.cursor();

    lcd.write("Initialized & Homed");
    lcd.home();
    std::this_thread::sleep_for(std::chrono::milliseconds(3000));
}
//----------------------------------------------------------------------------------------------------------------------
void fini(lcd::LcdStream const& lcd) {
    lcd.cursor(false);
    lcd.display(false);
}

