//======================================================================================================================
// 2021 Copyright Mystic Lake Software
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
//======================================================================================================================
//
//		 File: rvr_tcp.cpp
//
//     Author: rmerriam
//
//    Created: Jan 1, 2022
//
//======================================================================================================================
#include <cstdint>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <LcdStream.h>

using lcd::LcdStream;
using namespace std::literals;
//----------------------------------------------------------------------------------------------------------------------
#include <PiGpio.h>
#include <LcdI2c.h>
#include <LcdSerial.h>
//----------------------------------------------------------------------------------------------------------------------
void init(lcd::LcdStream const& lcd);
void fini(lcd::LcdStream const& lcd);
void backlight(lcd::LcdStream const& lcd);
void color_changes(lcd::LcdStream const& lcd);
void contrast_changes(lcd::LcdStream const& lcd);
void cursor_move(lcd::LcdStream const& lcd);
void custom_char(lcd::LcdStream const& lcd);
void display_move(lcd::LcdStream const& lcd);
//----------------------------------------------------------------------------------------------------------------------
char pi_addr[] { "rvr-pi2-lite" };
constexpr uint16_t base_timer { 1000 };
//----------------------------------------------------------------------------------------------------------------------
int main() {
    PiGpio pigpio { pi_addr };

    std::cout << "Hardware rev: " << pigpio.hardware_revision() << '\n';
    std::cout << "pigpio rev: " << pigpio.pigpio_version() << '\n';
    std::cout << "pigpio If rev: " << pigpio.if_version() << '\n';
    std::cout << '\n';

    lcd::LcdI2c lcd { pigpio };
//    lcd::LcdSerial lcd { pigpio};

    if ( !lcd()) {
        std::cout << "Could not open device. Error: " << lcd.error() << '-' << pigpio.error(lcd.error()) << '\n';
        return 1;
    }

    lcd.reset();
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    lcd.disableMessages();
    init(lcd);

    lcd.cursor();

//    contrast_changes(lcd);
//    color_changes(lcd);
//    backlight(lcd);
//    cursor_move(lcd);
//    display_move(lcd);
    custom_char(lcd);

    lcd.firmware();

    fini(lcd);
    return 0;
}

